'use strict';

/* Controllers */

var fidelMobileControllers = angular.module('fidelMobileControllers',  ['ngResource']);

fidelMobileControllers.controller('IndexCtrl', ['$scope','$state','$http','$ionicLoading', '$ionicPopup','fidelMobileServices', 'MissingFields', '$timeout',
  
  function($scope, $state, $http, $ionicLoading, $ionicPopup, fidelMobileServices, MissingFields, $timeout) {

        $scope.singin = function(data){
           $state.go("singin");
        };

        $scope.forgotPassword = function(data){
            $scope.showErrorMessage = false;
            $state.go("forgotpassword");

        };

        $scope.singup = function(data){
            $scope.user = {};
            $scope.showPasswords = true;
            $scope.user.isEmailSingup = true;
            $state.go("signup");
        };
		
		$scope.fbLogin = function(data){			  	
			  OAuth.popup('facebook').done(function(result) {				 				
				console.log(result);        	  
			    result.me().done(function(me) {
			    	$scope.showLoading(); 
			    	//Timeout busqueda de un fidelizado en el core 
			    	var myTimeOut = $timeout(function() {
			    		$scope.hideLoading();
			    		$scope.showAlert();
			    	}, 20000);			    	
			    	return fidelMobileServices.findByFacebookId(me.raw.id).then(function(response) {
			        	console.log(response)
			        	if (response.data.cliente == null){			        					        	
			        		$scope.user = {};	        					        	
			        		//$scope.user.empresaId = EMPRESA_ID;
			        		$scope.user.firstName = me.firstname;
			        		$scope.user.lastName = me.lastname;
			        		$scope.user.email = me.email;
			        		$scope.user.facebookId = me.id;
			        		$scope.showErrorMessage = false;
                            $scope.showPasswords = false;
                            $scope.user.isEmailSingup = false;
                            $state.go("signup");
			        	}else{			        		
			        		$scope.user = response.data.cliente;
			        		$state.go("welcome");
			        	}			
			        	$timeout.cancel(myTimeOut);
			    		$scope.hideLoading();			    	
			        })  
			    })		    
			})
		}
		
		$scope.twLogin = function(data){
					
			OAuth.popup('twitter').done(function(result) {
			    console.log(result);		
			    result.me().done(function(me) {			    	
			    	$scope.showLoading(); 			    	
			    	var myTimeOut = $timeout(function() {
			    		$scope.hideLoading();
			    		$scope.showAlert();
			    	}, 20000);			    	
			    	return fidelMobileServices.findByTwitterId(me.raw.id).then(function(response) {
			        	console.log(response)
			        	if (response.data.cliente == null){	
			        		$scope.user = {};		        					        	
			        		//$scope.user.empresaId = EMPRESA_ID;
			        		$scope.user.twitterId = me.raw.id;
                            $scope.user.isEmailSingup = false;
                            $scope.showErrorMessage = false;
                            $scope.showPasswords = false;
			        		$state.go("signup");
			        	}else{
			        		$scope.user = response.data.cliente;
			        		$state.go("welcome");
			        	}
			        	$timeout.cancel(myTimeOut);
			    		$scope.hideLoading();			    	
			        
			    	})	
			    })
			})
		  };
		  
		  $scope.saveUser = function(user){

              $scope.showLoading();
			
				var myTimeOut = $timeout(function() {
		    		$scope.hideLoading();
		    		$scope.showAlert();
		    	}, 20000);
				  
			  return fidelMobileServices.saveUser(user).then(function(response) {
		        	console.log(response)		        	  
		        	if (response.data.isValid == true ){
		        		$scope.showErrorMessage = false;
                        $scope.showPasswords = true;
			        	$scope.user = response.data.cliente;
                        $state.go("welcome");
		        	} else{	     
		        		$scope.errorMessages  = [];
		        		$scope.showErrorMessage = true;
		        		for (var i = 0; i < response.data.missingFields.length; i++){
		        			$scope.errorMessages.push($scope.getMissingFields (response.data.missingFields[i]));
		        		}		     	
		        	}			        				        		       	   
		        	$scope.hideLoading();
		        	$timeout.cancel(myTimeOut);
		     })	
		     	
		
		 };

          $scope.findUser = function(user){

              $scope.showLoading();

              var myTimeOut = $timeout(function() {
                  $scope.hideLoading();
                  $scope.showAlert();
              }, 20000);

              if (user == null || user.email == null || user.password == null || user.email == "" || user.password == ""){
                  $scope.errorMessages  = [];
                  $scope.showErrorMessage = true;
                  $scope.errorMessages.push("Ingrese usuario y password");
                  $timeout.cancel(myTimeOut);
                  $scope.hideLoading()
                  return;
              }

              return fidelMobileServices.findUser(user).then(function(response) {
                  console.log(response)
                  if (response.data.cliente == null){
                      //$scope.user.empresaId = EMPRESA_ID;
                      $scope.errorMessages  = [];
                      $scope.showErrorMessage = true;
                      $scope.errorMessages.push("Usuario o password incorrectos");
                  }else{
                      $scope.user = response.data.cliente;
                      $state.go("welcome");
                  }
                  $timeout.cancel(myTimeOut);
                  $scope.hideLoading();
              })


          };

          $scope.sendPassword = function(user){

              $scope.showLoading();

              var myTimeOut = $timeout(function() {
                  $scope.hideLoading();
                  $scope.showAlert();
              }, 20000);

              if (user == null){
                  $scope.errorMessages  = [];
                  $scope.showErrorMessage = true;
                  $scope.errorMessages.push("Ingrese su Email");
                  $timeout.cancel(myTimeOut);
                  $scope.hideLoading()
               return;
              }
              return fidelMobileServices.sendPassword(user).then(function(response) {
                  console.log(response)
                  if (response.data.cliente == null){
                      //$scope.user.empresaId = EMPRESA_ID;
                      $scope.errorMessages  = [];
                      $scope.showErrorMessage = true;
                      $scope.errorMessages.push("Usuario no registrado");
                  }else{
                      $scope.showErrorMessage = false;
                      $state.go("singin");
                  }
                  $timeout.cancel(myTimeOut);
                  $scope.hideLoading();
              })


          };

		 $scope.getMissingFields = function (key) {
			 return MissingFields.Fields.filter(function(field) {
			         return field.enumName === key;
			     })[0].name;
		};
		
		$scope.showAlert = function() {
  			   var alertPopup = $ionicPopup.alert({
  			     title: 'Ha ocurrido un error',
  			     template: 'Por favor intentelo mas tarde'
  			   });
		};
	     $scope.showLoading = function() {
			     $ionicLoading.show({
			    	 content: 'Loading',
					 animation: 'fade-in',
					 showBackdrop: true,
					 maxWidth: 200
			     });
		};
		$scope.hideLoading = function(){
			     $ionicLoading.hide();
	   };
	   
	}
]);





 