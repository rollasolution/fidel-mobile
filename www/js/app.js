// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'

var app = angular.module('starter', [                                    
	'ionic',
	'fidelMobileControllers',
	'fidelMobileServices'
	
]);
                                                                         	                                
app.config(function($stateProvider) {
  $stateProvider
  .state('home', {
    url: '/',
    templateUrl: 'index.html'
  })
  //TODO:Cambiar Carpera de singup a views
  .state('signup', {
    url: '/signup',
    templateUrl: 'signup.html'
  })
  .state('welcome', {
    url: '/welcome',
    templateUrl: 'welcome.html'
  })
   .state('singin', {
     url: '/singin',
     templateUrl: 'singin.html'
   })
   .state('forgotpassword', {
     url: '/forgot-password',
     templateUrl: 'forgot-password.html'
   });

});


app.run(function($ionicPlatform) {
  
	$ionicPlatform.ready(function() {
  
	OAuth.initialize('a9D-NR3RqaKSnnLKZnRhgkz7s84');
		
	// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

app.constant('MissingFields', {
	Fields : [				
				{ id: 1, enumName: 'NOMBRE', name: 'Nombre' },
		        { id: 2, enumName: 'APELLIDO', name: 'Apellido' },
		        { id: 3, enumName: 'FECHA_NAC', name: 'Fecha Nacimiento' },
		        { id: 4, enumName: 'SEXO', name: 'Sexo' },
		        { id: 5, enumName: 'LOC', name: 'Localidad' },
		        { id: 6, enumName: 'EMAIL', name: 'Email' },
		        { id: 7, enumName: 'ESTADO_CIVIL', name: 'Estado Civil' },
		        { id: 8, enumName: 'HIJOS', name: 'Cantidad de Hijos' },
		        { id: 9, enumName: 'TELEFONO_CELULAR', name: 'Telefono Celular' },
                { id: 10, enumName: 'CONFIRM_PASSWORD', name: 'Confirm Password' },
                { id: 11, enumName: 'PASSWORD', name: 'Password' },
                { id: 12, enumName: 'PASSWORD_NOT_MACH', name: 'Los campos password no coinciden' },
                { id: 13, enumName: 'USER_EXIST', name: 'El email esta registrado' }

    ]
});






