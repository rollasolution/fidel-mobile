/**
 * 
 */

var fidelMobileServices = angular.module('fidelMobileServices', ['ngResource']);

var FIDEL_CORE = 'http://2ff4358d.ngrok.com/fidel-core/rest'; //Localhost BDF 192.168.0.116
var EMPRESA_ID = 2;

fidelMobileServices.factory('fidelMobileServices', ['$resource', '$http', function($resource, $http){
    
    return {
	        findByTwitterId: function (twitterId) {
	            return $http({
	                method: 'GET',
	                url: FIDEL_CORE + "/clientes/twitterId/" + twitterId
	            });
	        },
		    findByFacebookId: function (facebookId) {
		        return $http({
		            method: 'GET',
		            url: FIDEL_CORE + "/clientes/facebookId/" + facebookId
		        });
		    },
		    saveUser: function (user) {		    	
		    		    	
		    	var userData = {"nombre":user.firstName, 
		    					"apellido":user.lastName, "fechaNacimiento":user.fechaNacimiento, "sexo": user.sexo, 
		    					"email": user.email, "estadoCivil": user.estadoCivil, "cantidadHijos": user.cantidadHijos,
		    					"telefonoCelular": user.telefonoCelular, "localidad": user.localidad, "facebookId": user.facebookId,
		    					"twitterId": user.twitterId, "password": user.password, "confirmPassword" : user.confirmPassword,
                                "isEmailSingup" : user.isEmailSingup} ;
		    	
		    	var clienteDto =  {"cliente": userData,  
		    					   "empresaId": EMPRESA_ID };
		    				    				    	
		        return $http({
		            method: 'PUT',
		            data: clienteDto,
		            url: FIDEL_CORE + "/clientes/save"
		        });
		    },
            findUser: function (user) {

                var userData = {"email": user.email, "password": user.password} ;

                var clienteDto =  {"cliente": userData,
                    "empresaId": EMPRESA_ID };

                return $http({
                    method: 'POST',
                    data: clienteDto,
                    url: FIDEL_CORE + "/clientes/find"
                });
            },

             sendPassword: function (user) {

                var userData = {"email": user.email} ;

                var clienteDto =  {"cliente": userData,
                    "empresaId": EMPRESA_ID };

                return $http({
                    method: 'POST',
                    data: clienteDto,
                    url: FIDEL_CORE + "/clientes/forgotPassword"
                });
            }

    
    
    };
}]);